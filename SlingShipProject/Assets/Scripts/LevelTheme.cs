﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelTheme : MonoBehaviour
{
    //Make this class singleton, so music don't restart in every level
    public static LevelTheme instance = null;

    //If exist another level theme, destroy itself
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
        else if(instance != this)
        {
            Destroy(gameObject);
        }
    }
}
