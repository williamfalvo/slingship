﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    #region Parameters
    [Range(-0.2f,0.2f), Tooltip("Wind force"), Header("Player Settings")]
    public float windForce;
    [Range(-0.2f,0f), Tooltip("Sea resistance force")]
    public float decraseForce;
    [Range(0f,3f), Tooltip("Ship speed")]
    public float shipSpeed;
    [Tooltip("Check if this level is a tutorial level")]
    public bool isTutorial;
    #endregion

    #region References
    [Tooltip("Player prefab reference"), Header("References"), Space(2)]
    public GameObject player;
    [Tooltip("Wind Arrow scene object reference")]
    public RectTransform windArrow;
    [Tooltip("Win panel scene object reference")]
    public GameObject winPanel;
    [Tooltip("Lose panel scene object reference")]
    public GameObject losePanel;
    [Tooltip("Win audio component reference")]
    public AudioSource winAudio;
    [Tooltip("Lose audio component  reference")]
    public AudioSource loseAudio;
    [Tooltip("Tutorial Manager scene object reference")]
    public TutorialManager tutorialManager;
    #endregion

    void Start()
    {
        //Start game if this is't a tutorial level
        if (tutorialManager != null)
        {
            tutorialManager.OnTutorialFinished += StartGame;
        }
        else
        {
            StartGame();
        }
        
        //Set orientation and color of wind arrow, based on wind force
        if(windForce == 0)
        {
            windArrow.gameObject.SetActive(false);
        }
        else
        {
            if (windForce < 0)
            {
                windArrow.localScale = new Vector3(-1, 1, 1);
            }

            if(Mathf.Abs(windForce) > 0.1f)
            {
                windArrow.GetComponent<Image>().color = Color.red;
            }
            else if(Mathf.Abs(windForce) > 0.05f)
            {
                windArrow.GetComponent<Image>().color = Color.yellow;
            }
                
        }
    }

    
    public void StartGame()
    {
        //Instantiate ship and set its parameters
        ShipController ship = Instantiate(player, Vector3.zero, Quaternion.identity).GetComponent<ShipController>();

        ship.windForce = new Vector3(windForce, 0, 0);
        ship.decreaseForce = new Vector3(0, 0, decraseForce);
        ship.speed = shipSpeed;
        ship.isTutorial = isTutorial;

        //Register to victory and lose events
        ship.OnVictory += Victory;
        ship.OnFailure += Failure;
        
    }

    public void Victory()
    {
        winPanel.SetActive(true);
        winAudio.Play();
    }

    public void Failure()
    {
        losePanel.SetActive(true);
        loseAudio.Play();
    }

    public void RestartScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void LoadNextLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        
    }

    public void LoadMainMenu()
    {
        Destroy(LevelTheme.instance.gameObject);
        SceneManager.LoadScene(0);
    }
}
