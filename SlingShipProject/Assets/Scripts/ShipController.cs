﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class ShipController : MonoBehaviour
{
    #region References
    [Tooltip("Child line renderer that show how much player pull the ship")]
    public LineRenderer jointLine;
    [Tooltip("Water movement child game object")]
    public GameObject waterVFX;
    
    LineRenderer lr;    //Trajectory component
    #endregion

    #region Parameters
    [HideInInspector]
    public float speed;
    [HideInInspector]
    public Vector3 velocity;
    [HideInInspector]
    public Vector3 windForce;
    [HideInInspector]
    public Vector3 decreaseForce;
    [HideInInspector]
    public Vector3[] trajectory;
    [HideInInspector]
    public bool isTutorial;

    [Tooltip("Total time to reach the end of trajectory")]
    public float totalTime;
    [Tooltip("Radius of charge pull action")]
    public float chargeRadius = 2f;
    [Tooltip("Min velocity to can move the ship")]
    public float minVelocity = 0.75f;

    bool isCharging;
    bool isMoving;
    bool canShoot;
    bool isHitted;
    int maxIndex;
    Vector3 startPosition;
    #endregion

    #region Events
    //Victory event to alert other that player win
    public delegate void Victory();
    public event Victory OnVictory;

    //Lose event to alert other that player lose
    public delegate void Failure();
    public event Failure OnFailure;
    #endregion


    void Start()
    {
        lr = GetComponent<LineRenderer>();
        lr.enabled = isTutorial;
        startPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        //If mouse clicked something on screen and ship is not moving, change to charge phase
        if (Input.GetMouseButtonDown(0) && !isMoving)
        {
            Ray toMouse = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(toMouse, out hit, 500.0f))
            {
                isCharging = true;
            }
        }

        //Charge phase 
        if (Input.GetMouseButton(0) && isCharging)
        {
            if (!isTutorial)
            {
                jointLine.SetPosition(1, transform.position);
            }

            //Check position of mouse on screen
            Ray toMouse = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(toMouse, out hit, 500.0f))
            {
                //Check if the new position of ship was pulled down of screen, else adjust it to 0
                Vector3 newPoint;

                if (hit.point.z > 0)
                {
                    newPoint = new Vector3(hit.point.x, transform.position.y, 0);
                }
                else
                {
                    newPoint = new Vector3(hit.point.x, transform.position.y, hit.point.z);
                }
                
                //Clamp new position with 2 of radius and set the velocity with a speed
                Vector3 point = Vector3.ClampMagnitude(newPoint, chargeRadius);
                transform.position = point;

                velocity = -point * speed;

                //Make ship to look always the start position during this phase
                transform.LookAt(startPosition);

                //Clear trajectory if velocity is under the minumum, else calculate trajectory and make shoot enabled
                if (velocity.z < minVelocity)
                {
                    canShoot = false;
                    lr.SetPositions(new Vector3[trajectory.Length]);
                    if (!isTutorial)
                    {
                        jointLine.enabled = false;
                    }
                }
                else
                {
                    canShoot = true;
                    CalculateTrajectory();
                    if (!isTutorial)
                    {
                        jointLine.enabled = true;
                    }
                }
                
            }
        }

        //Make ship move if we can shoot, else reset ship position 
        if (Input.GetMouseButtonUp(0) && isCharging)
        {
            isCharging = false;

            jointLine.enabled = false;

            if (canShoot)
            {
                isMoving = true;
                StartCoroutine(FollowTrajectory(0, trajectory[0]));
                waterVFX.SetActive(true);
                
            }
            else
            {
                lr.SetPositions(new Vector3[trajectory.Length]);
                transform.position = Vector3.zero;
                transform.LookAt(Vector3.forward);
            }

            
        }
    }


    void CalculateTrajectory()
    {
        Vector3 tmp_velocity = velocity;
        
        //Set first 2 positon to ship charge position and its start position
        Vector3 lastPosition = Vector3.zero;
        trajectory[0] = transform.position;
        trajectory[1] = lastPosition;

        //For every movement set 4 step 
        for (int i = 2; i < trajectory.Length; i+=4)
        {
            //Calculate next velocity step with all forces
            tmp_velocity += windForce + decreaseForce;

            //If velocity is under 0 set the last position as the last step
            if(tmp_velocity.z <= 0)
            {
                maxIndex = i - 4;
                for (int k = i; k < trajectory.Length; ++k)
                {
                    trajectory[k] = trajectory[maxIndex];
                }
                break;
            }

            maxIndex = trajectory.Length - 1;
            lastPosition += tmp_velocity;
            trajectory[i] = Vector3.Lerp(trajectory[i - 1], lastPosition, 0.25f);
            trajectory[i + 1] = Vector3.Lerp(trajectory[i - 1], lastPosition, 0.5f);
            trajectory[i + 2] = Vector3.Lerp(trajectory[i - 1], lastPosition, 0.75f);
            trajectory[i+3] = lastPosition;
        }

        //Set line renderer positions
        lr.positionCount = trajectory.Length;
        lr.SetPositions(trajectory);
    }


    IEnumerator FollowTrajectory(int index, Vector3 nextPosition)
    {
        WaitForEndOfFrame eof = new WaitForEndOfFrame();
        Vector3 startPos = transform.position;

        //Set time to every step of trajectory
        float animationAmount = 0.0f;
        float timeStep = totalTime / trajectory.Length;
        float speed = 1.0f / timeStep;

        //Move and rotate ship to next position 
        while (animationAmount < 1.0f)
        {
            animationAmount = Mathf.Min(1.0f, animationAmount + Time.deltaTime * speed);

            Vector3 newPosition = Vector3.Lerp(startPos, nextPosition, animationAmount);
            transform.LookAt(newPosition);
            transform.position = newPosition;
            
            yield return eof;            
        }

        //Go to next step of trajectory, but if we finish steps call lose event
        if(index < maxIndex)
        {
            StartCoroutine(FollowTrajectory(index + 1, trajectory[index + 1]));
        }
        else
        {
            OnFailure?.Invoke();
        }
    }

    
    void Hit()
    {
        isHitted = true;
        waterVFX.SetActive(false);
        StopAllCoroutines();
    }


    void OnTriggerEnter(Collider other)
    {
        //If ship touch tresure call win event
        if(other.gameObject.layer == 9 && !isHitted)
        {
            Hit();
            OnVictory?.Invoke();
        }
        //If ship touch obstacle call lose event
        else if (other.gameObject.layer == 10 && !isHitted)
        {
            Hit();
            OnFailure?.Invoke();
        }
    }
    
}
