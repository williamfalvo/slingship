﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

[System.Serializable]
public struct TutorialSignal
{
    [Tooltip("Tutorial index associate with")]
    public int index;
    [Tooltip("Object to be activated")]
    public GameObject signal;
}

public class TutorialManager : MonoBehaviour
{
    #region References
    [Tooltip("Component necessary to write text")]
    public TextMeshProUGUI tmp;
    #endregion

    #region Parameters
    [Tooltip("Phrases that appear during the tutorial phase")]
    public string[] phrases;
    [Tooltip("Signal that appears at 'index' during tutorial")]
    public TutorialSignal[] signals;
    int index;  //Index of current phrases of tutorial 
    #endregion

    #region Events
    public delegate void TutorialFinished();
    public event TutorialFinished OnTutorialFinished;
    #endregion

    // Start is called before the first frame update
    void Start()
    {        
        if (tmp == null) ExitTutorial();

        //Check if phrases is empty, else exit from tutorial phase
        if(phrases.Length > 0)
        {
            ControlSignals(index, true);
            tmp.text = phrases[index];
        }
        else
        {
            Debug.LogError("Phrases array is empty!");
            ExitTutorial();
        }
               
    }

    // Update is called once per frame
    void Update()
    {
        //Every left click increase index and check if tutorial is finished
        if (Input.GetMouseButtonDown(0))
        {
            ControlSignals(index, false);
            ++index;
            if(index < phrases.Length)
            {
                tmp.text = phrases[index];
                ControlSignals(index, true);
            }
            else
            {
                ExitTutorial();
            }
        }
    }

    void ControlSignals(int index, bool active)
    {
        //Check if index of tutorial is the same of some signals and activate/deactivate it 
        foreach(var s in signals)
        {
            if(s.index == index && s.signal != null)
            {
                s.signal.SetActive(active);
            }
        }
    }

    //Close tutorial panel and invoke tutorial finished event
    void ExitTutorial()
    {        
        gameObject.SetActive(false);
        OnTutorialFinished?.Invoke();
    }
}
