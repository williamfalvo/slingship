﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class ObstacleMoveBehaviour : MonoBehaviour
{
    [Tooltip("Distance from start position that object do")]
    public Vector3 moveDistance;
    [Tooltip("Movement speed")]
    public float speed;

    Rigidbody rb;
    Vector3 tempPos;    //Temporary position to check how much distance object done
    void Awake()
    {
        tempPos = transform.position;
        rb = GetComponent<Rigidbody>();
        rb.velocity = moveDistance.normalized * speed;
    }


    void Update()
    {
        //Check if reach the final position and invert it
        if ((transform.position - (tempPos + moveDistance)).sqrMagnitude <= 0.1f * 0.1f)
        {
            tempPos = tempPos + moveDistance;
            moveDistance *= -1;
            transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, transform.localScale.z * -1);
            rb.velocity = moveDistance.normalized * speed;
        }
    }
}
