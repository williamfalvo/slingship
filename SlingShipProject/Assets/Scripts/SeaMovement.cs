﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeaMovement : MonoBehaviour
{
    [Tooltip("Distance (Y Axis) that sea have to do")]
    public float moveDistance;
    Vector3 startPosition;

    void Start()
    {
        startPosition = transform.position;
    }
    
    void Update()
    {
        //Calculate new distance from start position and apply it on object
        float moveAmount = Mathf.Sin(Time.time) * moveDistance;
        transform.position = startPosition + new Vector3(0, moveAmount, 0);
    }
}
