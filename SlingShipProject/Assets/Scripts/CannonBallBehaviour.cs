﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonBallBehaviour : MonoBehaviour
{
    //When object is out of camera view, destroy itself
    void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
