﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonEffect : MonoBehaviour, IPointerEnterHandler
{
    [Tooltip("Button Highlighted sound")]
    public AudioSource highlightSfx;

    //When mouse is on this object play a sound
    public void OnPointerEnter(PointerEventData eventData)
    {
        //do your stuff when highlighted
        highlightSfx.Play();
    }
}
