﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class CannonBehaviour : MonoBehaviour
{
    #region References
    [Tooltip("Cannon ball prefab")]
    public GameObject cannonBall;
    [Tooltip("Smoke VFX prefab")]
    public GameObject smokeVfx;
    AudioSource shootSfx;
    #endregion

    #region Parameters
    [Tooltip("Time to start shooting")]
    public float startTime;
    [Tooltip("Cooldown necessary to prepare next shoot")]
    public float cooldown;
    [Tooltip("Horizontal speed of cannon ball")]
    public float shootSpeed;
    #endregion

    void Awake()
    {
        shootSfx = GetComponent<AudioSource>();
    }

    // Start is called before the first frame update
    void Start()
    {
        //Wait "startTime" seconds to start shooting
        Invoke("StartShoot", startTime);
    }

    void StartShoot()
    {
        StartCoroutine(ShootTimer());
    }

    IEnumerator ShootTimer()
    {
        yield return new WaitForSeconds(cooldown);

        //Instantiate a cannon ball and apply a velocity to it
        Rigidbody temp_rb = Instantiate(cannonBall, transform.position, Quaternion.identity).GetComponent<Rigidbody>();
        temp_rb.velocity = Vector3.right * shootSpeed;

        //Instantiate a smoke vfx
        Instantiate(smokeVfx, transform.position, Quaternion.identity);

        //Play a shoot sfx
        shootSfx.Play();

        //Prepare next shoot
        StartCoroutine(ShootTimer());
    }
}
