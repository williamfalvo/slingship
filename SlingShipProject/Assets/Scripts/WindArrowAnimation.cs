﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class WindArrowAnimation : MonoBehaviour
{
    Image rect;
    float runningTime;
    
    void Awake()
    {
        rect = GetComponent<Image>();
    }


    void Update()
    {
        rect.fillAmount += Time.deltaTime;

        if(rect.fillAmount == 1)
        {
            rect.fillAmount = 0;
        }
    }
}
